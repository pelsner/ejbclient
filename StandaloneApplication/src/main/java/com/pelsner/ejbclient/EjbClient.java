package com.pelsner.ejbclient;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.pelsner.interfaces.TestBeanRemote;

public class EjbClient {

   private Context jndiContext;
   private Context ejbRootContext;
   private TestBeanRemote testBean;

   private final static String testBeanEjbJndi = "ServerApplication/EjbModule/TestBean!com.pelsner.interfaces.TestBeanRemote";
   private final static String username = "";//należy podać login użytkownika na serwerze aplikacyjnym
   private final static String password = "";//należy podać hasło użytkonwika na serwerze aplikacyjnym
   private final static String host = "localhost";
   private final static String port = "4447";

   public EjbClient() {
      this.lookupEjb();
   }

   private void lookupEjb() {
      try {
         Properties jndiProperties = this.getEjbProperties();
         this.jndiContext = new InitialContext(jndiProperties);
         this.ejbRootContext = (Context) this.jndiContext.lookup("ejb:");
         this.testBean = (TestBeanRemote) this.ejbRootContext.lookup(EjbClient.testBeanEjbJndi);
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   public String getHelloWorld() {
      return this.testBean.getHelloWorld();
   }

   private Properties getEjbProperties() {
      final Properties properties = new Properties();
      properties.put("org.jboss.ejb.client.scoped.context", true);
      properties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
      final String connectionName = "ejbConnection";
      properties.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");
      properties.put("remote.connections", connectionName);
      properties.put("remote.connection." + connectionName + ".host", EjbClient.host);
      properties.put("remote.connection." + connectionName + ".port", EjbClient.port);
      properties.put("remote.connection." + connectionName + ".protocol", "remote");
//      properties.put("remote.connection." + connectionName
//                     + ".connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS", "true");
      properties.put("remote.connection." + connectionName + ".username", EjbClient.username);
      properties.put("remote.connection." + connectionName + ".password", EjbClient.password);
      properties.put("remote.connection." + connectionName
                     + ".connect.options.org.xnio.Options.SASL_DISALLOWED_MECHANISMS", "JBOSS-LOCAL-USER");

      properties.put("endpoint.name", connectionName);

      return properties;
   }
}
