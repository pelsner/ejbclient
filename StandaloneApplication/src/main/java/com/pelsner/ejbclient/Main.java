package com.pelsner.ejbclient;

public class Main {
   
   public static void main(String[] args) {
      new Main();
   }
   
   private Main() {
      EjbClient ejbClient = new EjbClient();
      System.out.println(ejbClient.getHelloWorld());
   }
}
