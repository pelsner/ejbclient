package com.pelsner.beans;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import com.pelsner.interfaces.TestBeanCommon;
import com.pelsner.interfaces.TestBeanLocal;
import com.pelsner.interfaces.TestBeanRemote;

@Stateless
@Local(TestBeanLocal.class)
@Remote(TestBeanRemote.class)
public class TestBean implements TestBeanCommon {

   @Override
   public String getHelloWorld() {
      return "Hello World ! :)";
   }

}
